let express = require('express');
const fs = require('fs');
const app = express();

const PORT = 80;

app.get('/video', (req,res) => {
    const path = "./videos/ddd.mp4";
    fs.stat(path, (err, stats) => {
        if(err)
        {
            console.error('an error ocurred');
            res.sendStatus(500);
            return;
        }
        
        res.writeHead(200, {
            'Content-Length' : stats.size,
            'Content-Type': "video/mp4"
        });

        //stream the video to the web
        fs.createReadStream(path).pipe(res);
    })
});

app.listen(PORT, ()=> {
    console.log(`Listening on port ${PORT}`);
});